const GetRatesAPI = require('./helpers/rates');
const csvdata = require('csvdata');

const currencies = ['EUR','USD','AUD','GBP'];
const days = 5;


const ratesApiCall = async () => {
    let responseRates = await GetRatesAPI.getHistoricalExchangeRates(days,currencies)
    let headerTitles = ['Currency'];
    let data = [];
    //Set Headers
    for (const key of Object.keys(responseRates)) {
        //Days
        headerTitles.push(key);
    }
    //Set Values
    for (const currencyValue of Object.values(currencies)) {
        if(currencyValue !== 'GBP' ){
          let tempObj = {
            Currency:currencyValue
          }
          for (const key of Object.keys(responseRates)) {
              tempObj[key] = responseRates[key][currencyValue];
          }
          data.push(tempObj);
        }
    }
   
    const csvHeaders = headerTitles.join(",");
    csvdata.write('./my-file.csv', data, {header: csvHeaders});
}
ratesApiCall();